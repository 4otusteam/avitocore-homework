using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AvitoCore.Migrations
{
    public partial class Migration9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Ad_CategoryId",
                table: "Ad",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Ad_PersonId",
                table: "Ad",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Ad_StatusId",
                table: "Ad",
                column: "StatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Ad_Category_CategoryId",
                table: "Ad",
                column: "CategoryId",
                principalTable: "Category",
                principalColumn: "CategoryId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Ad_Person_PersonId",
                table: "Ad",
                column: "PersonId",
                principalTable: "Person",
                principalColumn: "PersonId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Ad_Status_StatusId",
                table: "Ad",
                column: "StatusId",
                principalTable: "Status",
                principalColumn: "StatusId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ad_Category_CategoryId",
                table: "Ad");

            migrationBuilder.DropForeignKey(
                name: "FK_Ad_Person_PersonId",
                table: "Ad");

            migrationBuilder.DropForeignKey(
                name: "FK_Ad_Status_StatusId",
                table: "Ad");

            migrationBuilder.DropIndex(
                name: "IX_Ad_CategoryId",
                table: "Ad");

            migrationBuilder.DropIndex(
                name: "IX_Ad_PersonId",
                table: "Ad");

            migrationBuilder.DropIndex(
                name: "IX_Ad_StatusId",
                table: "Ad");
        }
    }
}
