using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AvitoCore.Migrations
{
    public partial class Migration5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "Person");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Person",
                type: "text",
                nullable: false,
                defaultValue: "");
        }
    }
}
