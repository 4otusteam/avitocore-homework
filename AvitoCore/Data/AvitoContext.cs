using AvitoCore.Models;
using Microsoft.EntityFrameworkCore;

namespace AvitoCore.Data
{
    public class AvitoContext : DbContext
    {
        /*public AvitoContext(DbContextOptions<AvitoContext> options) : base(options: options)
        {
        }*/

        public AvitoContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Status> Statuses { get; set; }        
        public DbSet<Ad> Ads { get; set; }


        /*public void ApplicationContext()
        {
            Database.EnsureCreated();
        }*/

        /*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=postgres;Username=postgres;Password=1");
        }*/

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {   
            modelBuilder.Entity<Person>().ToTable(name: "Person");
            modelBuilder.Entity<Category>().ToTable(name: "Category");
            modelBuilder.Entity<Status>().ToTable(name: "Status");
            modelBuilder.Entity<Ad>().ToTable(name: "Ad");
        }
    }
}