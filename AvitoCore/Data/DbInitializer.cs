#define FIRST
#if FIRST  // First DbInitializer used
#region snippet
using AvitoCore.Models;

namespace AvitoCore.Data
{
    public static class DbInitializer
    {
        public static void Initialize(AvitoContext context)
        {            
            if (context.Persons.Any())
            {
                Console.WriteLine("В базе найдены данные, работаем в штатном режиме");
                return;   // DB has been seeded
            }
            Console.WriteLine("Производится первичная инициализация базы");

            Person[] persons = new Person[]
            {
                new Person{PersonId = 1, LastName="Иванов",FirstName="Евлампий",MiddleName="Федорович",Email="ivanov231@mail.ru",Password="32ffsdv+_-0f"},
                new Person{PersonId = 2, LastName="Федоров",FirstName="Никита",MiddleName="Иванович",Email="fefor.nikita@gmail.com",Password="fjm*787"},
                new Person{PersonId = 3, LastName="Булатова",FirstName="Марфа",MiddleName="Васильевна",Email="bulatov342@mail.ru",Password="Jkfff75J"},
                new Person{PersonId = 4, LastName="Афанасьева",FirstName="Наталья",MiddleName="Андреевна",Email="afonia666@yandex.ru",Password="QQQWWD@$%"},
                new Person{PersonId = 5, LastName="Дугин",FirstName="Сергей",MiddleName="Андреевич",Email="duga334@gmail.com",Password="123Qwe-"},
                new Person{PersonId = 6, LastName="Козиков",FirstName="Иван",MiddleName="Васильевич",Email="kozik@yandex.ru",Password="bgKL9089vgvNnsdviosd"},
                new Person{PersonId = 7, LastName="Нестеров",FirstName="Денис",MiddleName="Александрович",Email="nester@yandex.ru",Password="vsdf345V"},
                new Person{PersonId = 8, LastName="Морозов",FirstName="Евгений",MiddleName="Васильевич",Email="moroz1990@gmail.com",Password="NJD^9fsdv"},
                new Person{PersonId = 9, LastName="Миронов",FirstName="Иван",MiddleName="Сергеевич",Email="miron123@mail.ru",Password="Viosjd777-="}
            };

            context.Persons.AddRange(entities: persons);
            context.SaveChanges();

            
            Category[] categories = new Category[]
            {
                new Category{CategoryId = 1, CategoryTitle="Авто"},
                new Category{CategoryId = 2, CategoryTitle="Бытовая техника"},
                new Category{CategoryId = 3, CategoryTitle="Компьютерная техника"},
                new Category{CategoryId = 4, CategoryTitle="Сотовые телефоны"},
                new Category{CategoryId = 5, CategoryTitle="Другое барахло"},
            };

            context.Categories.AddRange(entities: categories);
            context.SaveChanges();

            Status[] status = new Status[]
            {
                new Status{StatusId = 1, StatusTitle="активно"},
                new Status{StatusId = 2, StatusTitle="товар продан"},
                new Status{StatusId = 3, StatusTitle="товар снят с публикации пользователем"},
                new Status{StatusId = 4, StatusTitle="товар снят с публикации модератором"},
                new Status{StatusId = 5, StatusTitle="на модерации"}
            };

            context.Statuses.AddRange(entities: status);
            context.SaveChanges();

            
            Ad[] ads = new Ad[]
            {
                new Ad{PersonId = 1, CategoryId = 1, Text = "продам мопед", Price = 9999, StatusId = 1},
                new Ad{PersonId = 1, CategoryId = 1, Text = "продам машину", Price = 23432, StatusId = 2},
                new Ad{PersonId = 1, CategoryId = 2, Text = "продам тостер", Price = 754, StatusId = 1},
                new Ad{PersonId = 3, CategoryId = 3, Text = "продам оперативку ddr2", Price = 656, StatusId = 2},
                new Ad{PersonId = 3, CategoryId = 3, Text = "продам игровой ноутбук", Price = 325634, StatusId = 1},
                new Ad{PersonId = 4, CategoryId = 4, Text = "продам кнопочный телефон", Price = 2343, StatusId = 2},
                new Ad{PersonId = 5, CategoryId = 2, Text = "продам электровенчик(нерабочий)", Price = 34, StatusId = 1},
                new Ad{PersonId = 5, CategoryId = 2, Text = "продам умный пылесос", Price = 24239, StatusId = 1},
                new Ad{PersonId = 5, CategoryId = 2, Text = "продам чайник", Price = 3243, StatusId = 1},
                new Ad{PersonId = 5, CategoryId = 2, Text = "продам телевизор", Price = 9234, StatusId = 3},
                new Ad{PersonId = 7, CategoryId = 3, Text = "продам SSD", Price = 3333, StatusId = 3},
                new Ad{PersonId = 7, CategoryId = 5, Text = "продам какую-то неведомую штуку", Price = 5323, StatusId = 3},
                new Ad{PersonId = 9, CategoryId = 4, Text = "продам огрызок 12", Price = 40232, StatusId = 1},
                new Ad{PersonId = 9, CategoryId = 3, Text = "продам вебкамеру", Price = 4323, StatusId = 2},
                new Ad{PersonId = 9, CategoryId = 4, Text = "продам нерабочий смартфон", Price = 3243, StatusId = 1}
            };

            context.Ads.AddRange(entities: ads);
            context.SaveChanges();

            Console.WriteLine("База успешно проинициализирована");
        }
    }
}
#endregion
#endif