using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AvitoCore.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
                
        [StringLength(maximumLength: 100)]
        public string CategoryTitle { get; set; }
    }
}
