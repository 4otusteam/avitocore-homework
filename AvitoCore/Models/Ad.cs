using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AvitoCore.Models
{
    public class Ad
    {
        public int AdID { get; set; }

        public int PersonId { get; set; }
        public Person Person { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int StatusId { get; set; }
        public Status Status { get; set; }

        [StringLength(maximumLength: 1000)]
        public string Text { get; set; }
                
        public uint Price { get; set; }        
    }
}
