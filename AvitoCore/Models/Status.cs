using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AvitoCore.Models
{
    public class Status
    {
        public int StatusId { get; set; }

        [StringLength(maximumLength: 100)]
        public string StatusTitle { get; set; }
    }
}
