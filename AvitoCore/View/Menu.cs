using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvitoCore.View
{

    //отрисовка и базовая логика меню
    public static class Menu
    {
        private static readonly int row = Console.CursorTop;
        private static readonly int col = Console.CursorLeft;

        public static int MenuLogic(string[] items, int index)
        {
            while (true)
            {
                Menu.DrawMenu(items, index);
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.DownArrow:
                        if (index < items.Length - 1)
                            index++;
                            DrawMenu(items, index);
                        break;
                    case ConsoleKey.UpArrow:
                        if (index > 0)
                            index--;
                            DrawMenu(items, index);
                        break;
                    case ConsoleKey.Enter:
                        DrawMenu(items, index);
                        return index;
                }                
            }
        }

        public static void DrawMenu(string[] items, int index)
        {   Console.CursorVisible = false;
            Console.SetCursorPosition(col, row);
            for (int i = 0; i < items.Length; i++)
            {
                if (i == index)
                {
                    Console.BackgroundColor = Console.ForegroundColor;
                    Console.ForegroundColor = ConsoleColor.Black;
                }
                Console.WriteLine(items[i]);
                Console.ResetColor();
            }
            Console.WriteLine();
        }
    }
}
