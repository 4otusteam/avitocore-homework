using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvitoCore.View
{
    public static class PrintData
    {
        public static void Print(string Data, int Count)
        {
            switch (Count)
            {
                case <=5:
                    Console.Write($"{Data,-5}|", Data.Shorten(Count));
                    break;

                case <= 25:
                    Console.Write($"{Data,-25}|", Data.Shorten(Count));
                    break;

                default:
                    Console.Write($"{Data,-50}|", Data.Shorten(Count));
                    break;
            }            
        }
    }
}


//обрезка длинных строк для выравнивания таблицы
public static class StringHelper
{
    public static string Shorten(this string str, int value)
    {
        return
            value > 3 && str.Length > value ?
            str.Substring(0, value - 3) + "..." : str;
    }
}