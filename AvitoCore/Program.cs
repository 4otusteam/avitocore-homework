using AvitoCore.Data;
using AvitoCore.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AvitoCore.View;

public class Program
{

    static void Main(string[] args)
    {
        var applicationDbContextFactory = new AvitoDbContextFactory();
        using (var dbContext = applicationDbContextFactory.CreateDbContext(args))
        {
            //var ConnectionString = dbContext.Database.GetConnectionString;
            dbContext.Database.Migrate();
            DbInitializer.Initialize(dbContext);
            dbContext.Database.CloseConnection();
        }

        //menu
        string[] menuItems = new string[] { "Вывести данные", "Добавить данные", "Выход" };

        Console.WriteLine("\r\nВыберите действие:\r\n");

        int index = 0;
        while (true)
        {
            index = Menu.MenuLogic(menuItems, index);
            MainMenu(index, args);
        }

    }

    private static void MainMenu(int index, string[] args)
    {
        switch (index)
        {
            case 0:
                ViewData(args);
                break;
            case 1:
                AddRecordMenu(args);
                break;
            case 2:
                Environment.Exit(-1);
                break;
            default:
                break;
        }
    }

    private static void AddRecordMenu(string[] args)
    {
        string[] menuSubItemsAdd = new string[] { "Добавить пользователя", "Добавить категорию", "Назад" };
        int index = 0;

        var applicationDbContextFactory = new AvitoDbContextFactory();
        using (var dbContext = applicationDbContextFactory.CreateDbContext(args))
        {
            //var ConnectionString = dbContext.Database.GetConnectionString;            

            while (true)
            {
                index = Menu.MenuLogic(menuSubItemsAdd, index);
                switch (index)
                {
                    case 0:
                        AddUser(dbContext);
                        dbContext.Database.CloseConnection();
                        Console.Clear();
                        break;
                    case 1:
                        AddCategory(dbContext);
                        dbContext.Database.CloseConnection();
                        Console.Clear();
                        break;
                    case 2:
                        Console.Clear();
                        dbContext.Database.CloseConnection();
                        return;
                    default:
                        dbContext.Database.CloseConnection();
                        return;
                }
            }

        }
    }

    private static void AddUser(AvitoContext dbContext)
    {
        Person person = new Person();

        Console.Clear();
        Console.Write("\r\nВведите фамилию пользователя: ");
        person.LastName = Console.ReadLine();
        Console.Write("\r\nВведите имя пользователя: ");
        person.FirstName = Console.ReadLine();
        Console.Write("\r\nВведите отчество пользователя: ");
        person.MiddleName = Console.ReadLine();
        Console.Write("\r\nВведите email: ");
        person.Email = Console.ReadLine();
        Console.Write("\r\nВведите пароль: ");
        person.Password = Console.ReadLine();

        person.PersonId = dbContext.Persons.Count() + 1;

        dbContext.Persons.Add(person);
        dbContext.SaveChanges();
    }

    private static void AddCategory(AvitoContext dbContext)
    {
        Category category = new Category();

        Console.Clear();
        Console.Write("\r\nВведите название категории: ");
        category.CategoryTitle = Console.ReadLine();

        category.CategoryId = dbContext.Categories.Count() + 1;

        dbContext.Categories.Add(category);
        dbContext.SaveChanges();
    }



    private static void ViewData(string[] args)
    {
        var applicationDbContextFactory = new AvitoDbContextFactory();
        using (var dbContext = applicationDbContextFactory.CreateDbContext(args))
        {
            var ConnectionString = dbContext.Database.GetConnectionString;

            Console.WriteLine("______\r\nPerson\r\n______");
            var Person = dbContext.Persons;
            foreach (var person in Person)
            {
                PrintData.Print("\r\n", 0);
                PrintData.Print(person.PersonId.ToString(), 5);
                PrintData.Print($"{person.LastName} {person.MiddleName} {person.FirstName}", 100);
                PrintData.Print(person.Email, 25);
                PrintData.Print(person.Password, 25);
                PrintData.Print("\r\n", 0);
            }

            Console.WriteLine("\r\n________\r\nCategory\r\n________");
            var Category = dbContext.Categories;
            foreach (var category in Category)
            {
                PrintData.Print("\r\n", 0);
                PrintData.Print(category.CategoryId.ToString(), 5);
                PrintData.Print(category.CategoryTitle, 25);
                PrintData.Print("\r\n", 0);
            }

            Console.WriteLine("\r\n______\r\nStatus\r\n______");
            var Status = dbContext.Statuses;
            foreach (var status in Status)
            {
                PrintData.Print("\r\n", 0);
                PrintData.Print(status.StatusId.ToString(), 5);
                PrintData.Print(status.StatusTitle, 40);
                PrintData.Print("\r\n", 0);
            }

            dbContext.Database.CloseConnection();
        }
    }

    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
            => services.AddDbContext<AvitoContext>();

    }

    public class AvitoDbContextFactory : IDesignTimeDbContextFactory<AvitoContext>
    {

        public AvitoContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();

            var optionsBuilder = new DbContextOptionsBuilder();

            var connectionString = configuration
                        .GetConnectionString("DefaultConnection");

            optionsBuilder.UseNpgsql(connectionString);

            return new AvitoContext(optionsBuilder.Options);
        }
    }


}

